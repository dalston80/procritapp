<?php

require_once( 'BaseService.php' );
require_once( __LIB__.'/vo/ModuleVO.php' );

class ApplicationService extends BaseService
{
	/**
	 * Constructor.
	 *
	 * Contains Method Table metadata. Methods marked as remote in
	 * the Method Table may be called via the Amfphp Gateway.
	 */
	public function __construct() 
	{
		parent::__construct();
		
		// Require the method table generated by the Service Browser.
		include( __LIB__.'/ApplicationService.methodTable.php' );
	}
	
	/**
 	 * Returns a boolean value indicating whether the specified project is
 	 * enabled.
 	 *
 	 * @access remote
 	 * @param int The project ID.
 	 */
	public function getEnabled( $projectId )
	{
		$query = "SELECT inyEnabled as enabled"
			." FROM ".PROJECTS_TABLE
			." WHERE intProjectID = ".$projectId.";";
		
		$result = $this->dataSource->execute( $query );
		$project = $this->dataSource->nextRow( $result );
		
		if ( $project == NULL )
		{
			return false;
		}
		
		return $project[ 'enabled' ];
	}
   
	/**
 	 * Returns an array of all ModuleVO objects which the user has access to.
 	 *
 	 * @access remote
 	 * @param string The UDID of the device calling the method.
 	 */
	public function getModules( $udid, $projectId )
	{
		// Make sure the UDID exists in the database
		if ( $this->canExecute( $udid, $projectId ) == false )
		{
			return NULL;
		}
		
		$modulesQuery = "SELECT inyID as id, inyEnabled as enabled, chrName as name,"
				." chrArchiveURL as archiveURL, dtmUpdated as dateUpdated"
			." FROM ".MODULES_TABLE
			." WHERE intProjectID = ".$projectId
				." AND inyParentID IS NULL"
			." ORDER BY inyID ASC;";
				
		$modulesResult = $this->dataSource->execute( $modulesQuery );
		
		// This array will hold all the modules we are going to return
		$modulesArray = array();

		// Create an array or ModuleVOs from the returned Mysql resource
		while ( $module = $this->dataSource->nextRow( $modulesResult ) )
		{
		  	$moduleId = $module[ 'id' ];
			
		  	$newIndex = count( $modulesArray );
			array_push( $modulesArray, new ModuleVO() );

			$moduleVO = $modulesArray[ $newIndex ];

			$moduleVO->moduleId = $moduleId;
			$moduleVO->name = $module[ 'name' ];
			$moduleVO->enabled = (bool)$module[ 'enabled' ];
			$moduleVO->archiveURL = $module[ 'archiveURL' ];
			$moduleVO->dateUpdated = date('n/j/Y H:i:s', strtotime($module[ 'dateUpdated' ]));
			
			$moduleVO->childModules = $this->getModulesForParent( $projectId, $moduleId );
		}
		
		return $modulesArray;
	}
	
	/**
	 * Returns an array of enabled modules belonging to a specific parent.
	 * 
	 * @access private
	 */
	protected function getModulesForParent( $projectId, $parentId )
	{
		$modulesQuery = "SELECT inyID as id, inyEnabled as enabled, chrName as name,"
				." chrArchiveURL as archiveURL, dtmUpdated as dateUpdated"
			." FROM ".MODULES_TABLE
			." WHERE intProjectID = ".$projectId
				." AND inyParentID = ".$parentId
			." ORDER BY chrName ASC;";
		
		$modulesResult = $this->dataSource->execute( $modulesQuery );
		
		// This array will hold all the modules we are going to return
		$modulesArray = array();
		
		// Create an array or ModuleVOs from the returned Mysql resource
		while ( $module = $this->dataSource->nextRow( $modulesResult ) )
		{
		  	$newIndex = count( $modulesArray );
			array_push( $modulesArray, new ModuleVO() );

			$moduleVO = $modulesArray[ $newIndex ];

			$moduleVO->moduleId = $module[ 'id' ];
			$moduleVO->name = $module[ 'name' ];
			$moduleVO->enabled = $module[ 'enabled' ];
			$moduleVO->archiveURL = $module[ 'archiveURL' ];
			$moduleVO->dateUpdated = $module[ 'dateUpdated' ];
		}
		
		return $modulesArray;
	}
} 
?>