<?php
ini_set('display_errors', 'On'); // Debug only

if ( !defined( '__LIB__' ) )
{
	define( '__LIB__', Amfphp_ROOTPATH.'/lib/scientific_skyway' ); 
}

require_once( __LIB__.'/DataSource.php' );
include_once( __LIB__.'/config_db.php' );

class BaseService
{
	/**
	 * Properties
	 */
	protected $dataSource;
	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		// Initialize the data sources
		$this->dataSource = new DataSource( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
	}
	
	/**
 	 * Determines whether the user can execute a function.
 	 *
 	 * @param string The UDID of the device calling the method.
 	 */
	protected function canExecute( $udid, $projectId )
	{
		// Make sure the UDID exists in the database
   		$udidQuery = "SELECT *"
   			." FROM ".DEVICES_TABLE
   			." WHERE chrDeviceUDID = '".$udid
   			."' AND intProjectID = ".$projectId.";";
   		
		$udidCount = $this->dataSource->numberOfRows( $udidQuery );
		
		return ( $udidCount > 0 );
	}
	
	/**
	 * Returns the project ID for the user.
	 *
	 * @param int The user's ID.
	 */
	protected function getUsersProjectId( $userId )
	{
		// Make sure the UDID exists in the database
   		$query = "SELECT intProjectID as projectID "
   			." FROM ".USERS_TABLE
   			." WHERE intID = ".$userId.";";
   		
   		$result = $this->dataSource->execute( $query );
		$user = $this->dataSource->nextRow( $result );
		
		if ( $user != NULL )
		{
			return $user[ 'projectID' ];
		}
		
		return -1;
	}
}
?>