<?php 
$this->methodTable = array(
	"getTopScores" => array(
		"description" => "Returns an array of top scores for a game.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "gameId" => array( "type" => "int", "required" => true ), 
							  "count" => array( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"addScore" => array(
		"description" => "Adds a new score to a game.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "userId" => array( "type" => "string", "required" => true ), 
							  "gameId" => array( "type" => "int", "required" => true ), 
							  "initials" => array( "type" => "string", "required" => true ), 
							  "score" => array( "type" => "int", "required" => true ), 
							  "city" => array( "type" => "string", "required" => false ), 
							  "state" => array( "type" => "string", "required" => false ) ),
		"access" => "remote"
	)
);
?>