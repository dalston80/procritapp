<?php 
$this->methodTable = array(
	"getUsersForDevice" => array(
		"description" => "Returns an array of all users related to a device.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"createUser" => array(
		"description" => "Creates a new user.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "title" => array( "type" => "string", "required" => false ), 
							  "fullName" => array( "type" => "string", "required" => true ), 
							  "avatarURL" => array( "type" => "string", "required" => false ) ),
		"access" => "remote"
	),
	"updateUser" => array(
		"description" => "Updates an existing user.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "userId" => array( "type" => "int", "required" => true ), 
							  "title" => array( "type" => "string", "required" => false ), 
							  "fullName" => array( "type" => "string", "required" => false ), 
							  "avatarURL" => array( "type" => "string", "required" => false ) ),
		"access" => "remote"
	),
	"setUserProgress" => array(
		"description" => "Sets the user's current application progress.",
		"arguments" => array( "udid" => array( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "userId" => array( "type" => "int", "required" => true ), 
							  "moduleId" => array( "type" => "int", "required" => true ) ),
	"access" => "remote"
	)
);
?>