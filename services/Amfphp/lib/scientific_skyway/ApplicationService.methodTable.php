<?php 
$this->methodTable = array(
	"getEnabled" => array(
		"description" => "Returns a boolean value indicating whether the specified project is enabled.",
		"arguments" => array( "projectId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"getModules" => array(
		"description" => "Returns an array of all enabled ModuleVO objects which the user has access to.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ), 
							  "projectId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	)
);
?>