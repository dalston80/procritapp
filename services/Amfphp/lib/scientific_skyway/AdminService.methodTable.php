<?php 
$this->methodTable = array(
	"login" => array(
		"description" => "Logs a user into the admin system.",
		"arguments" => array( "userName" => array ( "type" => "string", "required" => true ),
							  "passwordHash" => array ( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"getProjects" => array(
		"description" => "Returns an array of all ProjectVO objects in the system.",
		"arguments" => array( "hash" => array ( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"setProjectEnabled" => array(
		"description" => "Enables/disabes a specific project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "enabled" => array( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"createProject" => array(
		"description" => "Creates a new project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectName" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"removeProject" => array(
		"description" => "Removes a specific project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"addDevice" => array(
		"description" => "Adds a new device to a specific project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "udid" => array( "type" => "string", "required" => true ),
							  "ownerName" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"updateDevice" => array(
		"description" => "Updates a specific device in the system.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "deviceId" => array( "type" => "int", "required" => true ),
							  "udid" => array( "type" => "string", "required" => true ),
							  "ownerName" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"removeDevice" => array(
		"description" => "Removes a specific device from a project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "udid" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"getDevicesForProject" => array(
		"description" => "Returns an array of all DeviceVO objects in the system for the given project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true )),
		"access" => "remote"
	),
	"addModule" => array(
		"description" => "Adds a module to a specific project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "moduleName" => array( "type" => "string", "required" => true ),
							  "parentId" => array( "type" => "int", "required" => false ),
							  "archiveURL" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"getModulesForProject" => array(
		"description" => "Returns an array of all ModuleVO objects in the system for the given project.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true )),
		"access" => "remote"
	),
	"updateModule" => array(
		"description" => "Updates a specific module in the system.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "moduleId" => array( "type" => "int", "required" => true ),
							  "moduleName" => array( "type" => "string", "required" => true ),
							  "enabled" => array( "type" => "int", "required" => true ),
							  "archiveURL" => array( "type" => "string", "required" => true ) ),
		"access" => "remote"
	),
	"removeModule" => array(
		"description" => "Removes a specific module.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "moduleId" => array( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"setProjectEnabled" => array(
		"description" => "Enables/disabes a specific module.",
		"arguments" => array( "hash" => array( "type" => "string", "required" => true ),
							  "projectId" => array( "type" => "int", "required" => true ),
							  "moduleId" => array( "type" => "int", "required" => true ),
							  "enabled" => array( "type" => "int", "required" => true ) ),
		"access" => "remote"
	)
);
?>