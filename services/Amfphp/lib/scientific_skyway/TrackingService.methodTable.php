<?php 
$this->methodTable = array(
	"onLearningVisited" => array(
		"description" => "Marks a Learning module as visited for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onLearningTimeIncrement" => array(
		"description" => "Increments the time spent on a Learning module for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ),
							  "minutes" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onLearningCompleted" => array(
		"description" => "Marks a Learning module as completed for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onResourcesVisited" => array(
		"description" => "Marks a Resources module as visited for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onResourcesTimeIncrement" => array(
		"description" => "Increments the time spent on a Resources module for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ),
							  "minutes" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onResourcesSentPDF" => array(
		"description" => "Marks a PDF in a Resources module as sent for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onResourcesOpenedPDF" => array(
		"description" => "Marks a PDF in a Resources module as opened for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onChallengeVisited" => array(
		"description" => "Marks a Challenge module as visited for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ),
							  "minutes" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	),
	"onChallengeTimeIncrement" => array(
		"description" => "Increments the time spent on a Challenge module for the user.",
		"arguments" => array( "udid" => array ( "type" => "string", "required" => true ),
							  "projectId" => array ( "type" => "int", "required" => true ),
							  "moduleId" => array ( "type" => "int", "required" => true ),
							  "userId" => array ( "type" => "int", "required" => true ),
							  "minutes" => array ( "type" => "int", "required" => true ) ),
		"access" => "remote"
	)
);
?>