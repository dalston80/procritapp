<?php
class ModuleVO
{
	/**
	 * Properties
	 */
  	var $moduleId;		// int
	var $name; 			// string
	var $enabled;		// bool
	var $archiveURL; 	// string
	var $childModules;	// array
	var $dateUpdated;	// string
	
	/**
	 * Constructor.
	 */
	function __construct()
	{
		$childModules = array();
	}
}
?>