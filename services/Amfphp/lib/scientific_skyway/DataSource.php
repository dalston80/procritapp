<?php
class DataSource
{
	/**
	 * Properties
	 */
	var $connection;
	
	/**
	 * Constructor.
	 */
	function DataSource($host, $user, $password, $name)
	{
	  	// Connect to the database server
		$this->connection = mysql_connect($host, $user, $password)
			or trigger_error('Error connecting to database server: '.mysql_error($this->connection));
			
		// Select the database
		mysql_select_db($name, $this->connection)
			or trigger_error('Error selecting database: '.mysql_error($this->connection));
	}
	
	/**
	 * Executes the SQL query.
	 */
	function execute($query)
	{
		$result = mysql_query($query, $this->connection) 
			or trigger_error('Error executing query: '.mysql_error($this->connection) . 'Query: ' . $query);

		return $result;
	}
	
	/**
	 * Returns the number of rows affected by the query.
	 */
	function numberOfRows($query)
	{
		return mysql_num_rows($this->execute($query));
	}
	
	/**
	 * Returns the next row in a record set.
	 */
	function nextRow($result)
	{
		return mysql_fetch_assoc($result);
	}
	
	/**
	 * Escapes the string and makes it suitable for insertion in an SQL database.
	 */
	function escape($string)
	{
		return mysql_real_escape_string($string);
	}
	
	/**
	 * Returns the ID of the last inserted row.
	 */
	function getInsertId()
	{
	  	return mysql_insert_id();
	}
}
?>