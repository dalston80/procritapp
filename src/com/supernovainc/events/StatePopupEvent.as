package com.supernovainc.events
{
	import flash.events.Event;
	
	public class StatePopupEvent extends Event
	{
		public static const SHOW_STATES:String = "show_states";
		public static const HIDE_STATES:String = "hide_states";
		public var hsRenderer:Object;
		
		public function StatePopupEvent(type:String, child:Object)
		{
			super(type, true, false);
			this.hsRenderer = child;
		}
		
		override public function clone():Event
		{
			return new StatePopupEvent(SHOW_STATES, hsRenderer);
		}
		
		override public function toString():String
		{
			return formatToString("StatePopupEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}