package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class LeaderBoardEvent extends Event
	{
		public static const TOP_SCORES:String = "top_scores";
		public static const ADD_SCORE:String = "add_score";
		
		public var udid:String;
		public var projectID:int;
		public var gameID:String;
		public var userID:int;
		public var initials:String;
		public var score:int;
		public var city:String;
		public var state:String;
		public var count:int;
		public var navigator:ViewNavigator;
		public var scoreObj:Object;
		
		
		public function LeaderBoardEvent(type:String, id:String, pid:int, gid:String, uid:int, init:String=null, scr:int=0, ct:String=null, st:String=null, cn:int=0, vNav:ViewNavigator=null, sObj:Object=null)
		{
			super(type, true, false);
			udid = id;
			projectID = pid;
			gameID = gid;
			userID = uid;
			initials = init;
			score = scr;
			city = ct;
			state = st;
			count = cn;
			navigator = vNav;
			scoreObj = sObj;
		}
		override public function clone():Event
		{
			return new LeaderBoardEvent(type, udid, projectID, gameID, userID, initials, score, city, state, count, navigator, scoreObj);
		}
		override public function toString():String
		{
			return formatToString("LeaderBoardEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}