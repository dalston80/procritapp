package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class LearningEvent extends Event
	{
		public static const GO_LEARNING:String = "go_learning";
		
		public var learningData:Object;
		
		
		public function LearningEvent(type:String, ldata:Object)
		{
			super(type, true, false);
			
			learningData = ldata;
			
		}
		override public function clone():Event
		{
			return new LearningEvent(type, learningData);
		}
		override public function toString():String
		{
			return formatToString("LearningEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}