package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class ApplicationModuleEvent extends Event
	{
		public static const GET_MODULES:String = "get_modules";
		public static const PROJECT_ENABLED:String = "project_enabled";
		
		public var devUDID:String;
		public var userObj:Object;
		public var navigator:ViewNavigator;
		public var projectID:int;
		
		public function ApplicationModuleEvent(type:String, udid:String = null, pid:int = 0, uobj:Object = null, vnavigator:ViewNavigator = null)
		{
			super(type, true, false);
			
			projectID = pid;
			devUDID = udid;
			userObj = uobj;
			navigator = vnavigator;
		}
		override public function clone():Event
		{
			return new ApplicationModuleEvent(type, devUDID, projectID, userObj, navigator);
		}
		override public function toString():String
		{
			return formatToString("ApplicationModuleEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}