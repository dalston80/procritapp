package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class UserEvent extends Event
	{
		public static const GET_USERS:String = "get_users";
		public static const REFRESH_USERS:String = "refresh_users";
		public static const CREATE_USER:String = "create_user";
		public static const UPDATE_USER:String = "edit_user";
		public static const REMOVE_USER:String = "remove_user";
		public static const SET_PROGRESS:String = "set_progress";
		
		public var projectID:int;
		public var devUDID:String;
		public var userID:int;
		public var fullName:String;
		public var title:String;
		public var avatarURL:String;
		public var navigator:ViewNavigator;
		public var progress:int;
		
		public function UserEvent(type:String, pid:int, id:String, vnavigator:ViewNavigator = null, userid:int = 0, title:String = null, name:String = null, avatar:String = null, prog:int = 0)
		{
			super(type, true, false);
			this.projectID = pid;
			this.devUDID = id;
			this.userID = userid;
			this.fullName = name;
			this.title = title;
			this.avatarURL = avatar;
			this.navigator = vnavigator;
			this.progress = prog;
		}
		
		override public function clone():Event
		{
			return new UserEvent(type, projectID, devUDID, navigator, userID, title, fullName, avatarURL, progress);
		}
		
		override public function toString():String
		{
			return formatToString("UserEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}