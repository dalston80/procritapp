package com.supernovainc.events
{
	import flash.events.Event;
	
	public class TitleEvent extends Event{
		
		public static const GET_TITLES:String = "get_titles";
		
		public function TitleEvent(type:String)
		{
			super(type, true, false);
		}
		
		override public function clone():Event
		{
			return new TitleEvent(GET_TITLES);
		}
		
		override public function toString():String
		{
			return formatToString("TitleEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
		
	}
	
}