package com.supernovainc.events
{
	import flash.events.Event;
	
	public class SurveyEvent extends Event{
		
		public static const GET_QUESTIONS:String = "get_questions";
		
		public function SurveyEvent(type:String)
		{
			super(type, true, false);
		}
		
		override public function clone():Event
		{
			return new SurveyEvent(GET_QUESTIONS);
		}
		
		override public function toString():String
		{
			return formatToString("SurveyEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
		
	}
	
}