package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class ResourceEvent extends Event
	{
		public static const GO_RESOURCE:String = "go_resource";
		public static const GET_LINKS:String = "get_links";
		
		public var resourceData:Object;
		
		public function ResourceEvent(type:String, rData:Object)
		{
			super(type, true, false);
			
			resourceData = rData;
			
		}
		override public function clone():Event
		{
			return new ResourceEvent(type, resourceData);
		}
		override public function toString():String
		{
			return formatToString("ResourceEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}