package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class EmailEvent extends Event
	{
		
		public static const SEND_EMAIL:String = "send_email";
		
		public var answers:Object;
		public var navigator:ViewNavigator;
		
		public function EmailEvent(type:String, ans:Object, nav:ViewNavigator)
		{
			super(type, true, false);
			
			answers = ans;
			navigator = nav;
		}
		
		override public function clone():Event
		{
			return new EmailEvent(type, answers, navigator);
		}
		
		override public function toString():String
		{
			return formatToString("EmailEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}