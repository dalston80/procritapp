package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class ApplicationStartEvent extends Event
	{
		public static const APP_START:String = "app_start";
		public static const GET_MODULES:String = "get_modules";
		
		public var devUDID:String;
		public var projectID:int;
		
		public var navigator:ViewNavigator;
		
		public function ApplicationStartEvent(type:String, vnavigator:ViewNavigator = null)
		{
			super(type, true, false);
			navigator = vnavigator;
		}
	}
}