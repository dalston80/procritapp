package com.supernovainc.events
{
	import flash.events.Event;
	
	public class UDIDEvent extends Event{
		
		public static const GET_UDID:String = "get_udid";
		
		public function UDIDEvent(type:String)
		{
			super(type, true, false);
		}
		
		override public function clone():Event
		{
			return new UDIDEvent(GET_UDID);
		}
		
		override public function toString():String
		{
			return formatToString("UDIDEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
		
	}
	
}