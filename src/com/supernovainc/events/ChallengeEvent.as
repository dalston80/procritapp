package com.supernovainc.events
{
	import flash.events.Event;
	
	import spark.components.ViewNavigator;
	
	public class ChallengeEvent extends Event
	{
		public static const GO_CHALLENGE:String = "go_challenge";
		
		public var challengeData:Object;
		//public var navigator:ViewNavigator;
		
		public function ChallengeEvent(type:String, cData:Object)
		{
			super(type, true, false);
			
			challengeData = cData;
			
			
		}
		override public function clone():Event
		{
			return new ChallengeEvent(type, challengeData);
		}
		override public function toString():String
		{
			return formatToString("ChallengeEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}