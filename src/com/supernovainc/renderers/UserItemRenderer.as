package com.supernovainc.renderers
{
	import spark.components.Group;
	import spark.components.LabelItemRenderer;
	import spark.components.supportClasses.StyleableTextField;
	import spark.core.ContentCache;
	import spark.primitives.BitmapImage;
	
	public class UserItemRenderer extends LabelItemRenderer
	{
		
		private var _fullName:String;
		private var _title:String;
		private var _avatarURL:String;
		private var _userID:int;
		private var _progress:int;
		private var _userAvatar:BitmapImage;
		private var _userBg:BitmapImage;
		private var _userBgTouch:BitmapImage;
		private var titleText:StyleableTextField;
		private var fullNameText:StyleableTextField;
		
		private var bgGroup:Group;
		
		static public const myCache:ContentCache = new ContentCache();
		
		[Embed(source="assets/Profile screens/box.png")]
		private var box:Class;
		
		[Embed(source="assets/Profile screens/box_touch.png")]
		private var boxTouch:Class;
		
		public function UserItemRenderer()
		{
			super();
		}
		
		
		/* userID
		Sets or gets the user's ID
		*/
		public function set userID(value:int):void{
			if(value == _userID){
				return;
			}
			
			_userID = value;
		}
		
		public function get userID():int{
			return _userID;
		}
		/*end userID */
		
		
		
		
		/* userProgress
		Sets or gets the user's progress
		*/
		public function set userProgress(value:int):void{
			if(value == _progress){
				return;
			}
			
			_progress = value;
		}
		
		public function get userProgress():int{
			return _progress;
		}
		/* end userProgress */
		
		
		
		
		
		/* fullName
		Set the fullName of the user
		*/
		public function set fullName(value:String):void{
			if(value == _fullName){
				return;
			}
			
			_fullName = value;
			
			if(fullNameText){
				fullNameText.text = _fullName;
				invalidateSize();
			}
		}
		
		public function get fullName():String{
			return _fullName;
		}
		/*end fullName */
		
		
		
		
		
		/* title
		Set the title of the user
		*/
		public function set title(value:String):void{
			if(value == _title){
				return;
			}
			
			_title = value;
			
			if(titleText){
				titleText.text = _title;
				invalidateSize();
			}
		}
		
		public function get title():String{
			return _title;
		}
		/* end title */
		
		
		
		
		/* avatarURL
		Set the avatar of the user
		*/
		public function set avatarURL(value:String):void{
			
			// If the avatar is already set, exit this function...
			if(value == _avatarURL){
				return;
			}
			
			// ...Otherwise set the icon
			_avatarURL = value;
			
			
			if(_userAvatar){
				_userAvatar.source = _avatarURL;
				_userAvatar.contentLoader = myCache;
				invalidateSize();
			}
		}
		
		// Return the icon's value
		public function get avatarURL():String{
			return _avatarURL;
		}
		/* end avatarURL */
		
		
		
		
		// Set the data from the list in HomeProfileView.mxml (located in the views folder)
		override public function set data(value:Object):void{
			super.data = value;
			
			if(!data){
				return;
			}
			
			var tempID:int = data.userId;
			var tempProgress:int = data.progress;
			var tempFullName:String = data.fullName;
			var tempTitle:String = data.title;
			var tempAvatar:String = data.avatarURL;
			
			// Set the properties of this renderer
			userID = tempID;
			userProgress = tempProgress;
			fullName = tempFullName;
			title = tempTitle;
			avatarURL = tempAvatar;
		}
		
		override protected function createChildren():void{
			//labelDisplay.visible = false;
			
			
			// Create the components necessary to build this item renderer
			bgGroup = new Group();
			bgGroup.setStyle("horizontalCenter", 0);
			bgGroup.setStyle("verticalCenter", 0);
			addChild(bgGroup);
			
			_userBgTouch = new BitmapImage();
			_userBgTouch.source = boxTouch;
			_userBgTouch.alpha = 0;
			bgGroup.addElement(_userBgTouch);
			
			_userBg = new BitmapImage();
			_userBg.source = box;
			bgGroup.addElement(_userBg);
			
			_userAvatar = new BitmapImage();
			bgGroup.addElement(_userAvatar);
			
			fullNameText = new StyleableTextField();
			fullNameText.styleName = this;
			fullNameText.editable = false;
			fullNameText.selectable = false;
			fullNameText.multiline = false;
			fullNameText.wordWrap = false;
			fullNameText.setStyle("color", 0xFFFFFF);
			fullNameText.cacheAsBitmap = true;
			addChild(fullNameText);
			
			titleText = new StyleableTextField();
			titleText.styleName = this;
			titleText.editable = false;
			titleText.selectable = false;
			titleText.multiline = false;
			titleText.wordWrap = false;
			titleText.setStyle("color", 0xFFFFFF);
			titleText.cacheAsBitmap = true;
			addChild(titleText);
		}
		
		override protected function measure():void{
			// Set the size of the entire renderer
			measuredWidth = 193;
			measuredHeight = 193 + (fullNameText.height + titleText.height);
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
		
		override protected function layoutContents(unscaledWidth:Number, unscaledHeight:Number):void{
			
			// Position each part of the renderer invdividually
			setElementPosition(bgGroup, 0, 0);
			setElementPosition(_userAvatar, 0, 0);
			setElementPosition(fullNameText, 10, 195);
			setElementSize(fullNameText, 193, unscaledHeight);
			setElementPosition(titleText, 10, 215);
			setElementSize(titleText, 193, unscaledHeight);
		}
	}
}