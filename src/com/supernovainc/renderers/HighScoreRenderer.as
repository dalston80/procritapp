package com.supernovainc.renderers
{
	
	import com.supernovainc.events.LeaderBoardEvent;
	import com.supernovainc.events.StatePopupEvent;
	import com.supernovainc.skin.StateDropDownButtonSkin;
	import com.supernovainc.skin.SubmitScoreButtonSkin;
	
	import flash.events.MouseEvent;
	
	import spark.components.Button;
	import spark.components.Group;
	import spark.components.LabelItemRenderer;
	import spark.components.TextInput;
	import spark.components.ViewNavigator;
	import spark.components.supportClasses.StyleableTextField;
	import spark.primitives.BitmapImage;
	
	
	/**
	 * 
	 * ASDoc comments for this item renderer class
	 * 
	 */
	public class HighScoreRenderer extends LabelItemRenderer
	{
		
		private var _navigator:ViewNavigator;
		private var _id:int;
		private var _projectID:int;
		private var _udid:String;
		private var _userID:int;
		private var _gameID:String;
		private var _initials:String;
		private var _city:String;
		private var _state:String;
		private var _score:int;
		
		
		private var initialsInputText:TextInput;
		private var cityInputText:TextInput;
		public var stateInputText:TextInput;
		private var initialsDisplayText:StyleableTextField;
		private var cityDisplayText:StyleableTextField;
		private var stateDisplayText:StyleableTextField;
		private var scoreDisplayText:StyleableTextField;
		
		private var stateListButton:Button;
		private var submitScoreButton:Button;
		
		private var hsHighlight:BitmapImage;
		private var initialBg:BitmapImage;
		private var cityBg:BitmapImage;
		
		private var bgGroup:Group;
		
		[Embed(source="assets/Challenge/High Score/initial_box.png")]
		private var initialBox:Class;
		
		[Embed(source="assets/Challenge/High Score/city_box.png")]
		private var cityBox:Class;
		
		/*[Embed(source="assets/Challenge/High Score/state_dropdown.png")]
		private var stateDropDown:Class;*/
		
		[Embed(source="assets/Challenge/High Score/High_Scores_highlight.png")]
		private var highScoreHighlight:Class;
		
		
		
		public function HighScoreRenderer()
		{
			//TODO: implement function
			super();
			
		}
		// The view navigator reference needed to switch views when necessary
		public function set navigator(value:ViewNavigator):void{
			if(value == _navigator){
				return
			}
			
			_navigator = value;
		}
		
		public function get navigator():ViewNavigator{
			return _navigator;
		}
		
		
		
		public function set sid(value:int):void{
			if(value == _id){
				return;
			}
			
			_id = value;
		}
		
		public function get sid():int{
			return _id;
		}
		
		
		// The id of this project 
		public function set projectID(value:int):void{
			if(value == _projectID){
				return;
			}
			
			_projectID = value;
		}
		
		public function get projectID():int{
			return _projectID;
		}
		
		
		// The identifier of the iPad running the application
		public function set udid(value:String):void{
			if(value == _udid){
				return;
			}
			
			_udid = value;
		}
		
		public function get udid():String{
			return _udid;
		}
		
		
		
		// The currentUser of the application
		public function set userID(value:int):void{
			if(value == _userID){
				return;
			}
			
			_userID = value;
		}
		
		public function get userID():int{
			return _userID;
		}
		
		
		// The id of the game currently being played
		public function set gameID(value:String):void{
			if(value == _gameID){
				return;
			}
			
			_gameID = value;
		}
		
		public function get gameID():String{
			return _gameID;
		}
		
		
		// The initials of the player
		public function set initials(value:String):void{
			
			if(value == _initials){
				return;
			}
			
			_initials = value;
			
			if(initialsDisplayText){
				initialsDisplayText.text = _initials.toUpperCase();
				invalidateSize();
			}
		}
		
		public function get initials():String{
			return _initials;
		}
		
		
		// The city of the player
		public function set city(value:String):void{
			if(value == _city){
				return;
			}
			
			_city = value;
			
			if(cityDisplayText){
				cityDisplayText.text = _city;
				invalidateSize();
			}
		}
		
		public function get city():String{
			return _city;
		}
		
		
		// The state of the player
		public function set state(value:String):void{
			if(value == _state){
				return;
			}
			
			_state = value;
			
			if(stateDisplayText){
				stateDisplayText.text = _state.toUpperCase();
				invalidateSize();
			}
		}
		
		public function get state():String{
			return _state;
		}
		
		
		
		// The player's score
		public function set score(value:int):void{
			if(value == _score){
				return;
			}
			
			_score = value;
			
			if(scoreDisplayText){
				scoreDisplayText.text = String(_score);
				invalidateSize();
			}
		}
		
		public function get score():int{
			return _score;
		}
		
		
		
		
		/**
		 * @private
		 *
		 * Override this setter to respond to data changes
		 */
		override public function set data(value:Object):void
		{
			super.data = value;
			// the data has changed.  push these changes down in to the 
			// subcomponents here    	
			trace("data is set first or....");
			if(!data){
				return;
			}
			
			
			// If any of these data fields are empty, make them editable
			// so that the player can enter the rest of their high score
			// information. Otherwise, just display the current highscore
			if(data.initials == "" && data.city == "" && data.state == ""){
				
				score = data.score;
				navigator = data.navigator;
				udid = data.udid;
				projectID = data.projectID;
				userID = data.userID;
				gameID = data.gameID;
				bgGroup.visible = true;
				initialsInputText.visible = true;
				cityInputText.visible = true;
				stateInputText.visible = true;
				scoreDisplayText.visible = true;
				submitScoreButton.visible = true;
				stateListButton.visible = true;
				
			}else {
				
				
				initials = data.initials;
				city = data.city;
				state = data.state;
				score = data.score;
				
				initialsDisplayText.visible = true;
				cityDisplayText.visible = true;
				stateDisplayText.visible = true;
				scoreDisplayText.visible = true;
			}
			
		} 
		
		/**
		 * @private
		 * 
		 * Override this method to create children for your item renderer 
		 */	
		override protected function createChildren():void
		{
			
			// Create the children for this renderer
			
				
				if(!bgGroup){
					bgGroup = new Group();
					bgGroup.visible = false;
					addChild(bgGroup);
					//trace("bgGroup created");
				}
				
				if(!hsHighlight){
					hsHighlight = new BitmapImage();
					hsHighlight.x = 0;
					hsHighlight.y = 10;
					hsHighlight.source = highScoreHighlight;
					bgGroup.addElement(hsHighlight);
				}
				
				if(!initialBg){
					initialBg = new BitmapImage();
					initialBg.x = 140;
					initialBg.y = 0;
					initialBg.source = initialBox;
					bgGroup.addElement(initialBg);
				}
				
				if(!cityBg){
					cityBg = new BitmapImage();
					cityBg.x = 250;
					cityBg.y = 0;
					cityBg.source = cityBox;
					bgGroup.addElement(cityBg);
				}
				
				if(!initialsInputText){
					initialsInputText = new TextInput();
					initialsInputText.visible = false;
					initialsInputText.setStyle("focusAlpha", 0);
					initialsInputText.setStyle("color", 0xFFFFFF);
					initialsInputText.setStyle("borderVisible", false);
					addChild(initialsInputText);
				}
				
				if(!cityInputText){
					cityInputText = new TextInput();
					cityInputText.visible = false;
					cityInputText.setStyle("focusAlpha", 0);
					cityInputText.setStyle("color", 0xFFFFFF);
					cityInputText.setStyle("borderVisible", false);
					addChild(cityInputText);
				}
				
				if(!stateInputText){
					stateInputText = new TextInput();
					stateInputText.visible = false;
					stateInputText.focusEnabled = false;
					stateInputText.mouseEnabled = false;
					stateInputText.setStyle("focusAlpha", 0);
					stateInputText.setStyle("color", 0xFFFFFF);
					stateInputText.setStyle("borderVisible", false);
					addChild(stateInputText);
				}
				
				if(!stateListButton){
					stateListButton = new Button();
					stateListButton.visible = false;
					stateListButton.setStyle("skinClass", StateDropDownButtonSkin);
					stateListButton.addEventListener(MouseEvent.MOUSE_DOWN, stateClick);
					addChild(stateListButton);
				}
				
				if(!scoreDisplayText){
					scoreDisplayText = new StyleableTextField();
					scoreDisplayText.visible = false;
					scoreDisplayText.setStyle("color", "0xFFFFFF");
					addChild(scoreDisplayText);
				}
				
				if(!initialsDisplayText){
					initialsDisplayText = new StyleableTextField();
					initialsDisplayText.setStyle("color", "0xFFFFFF");
					initialsDisplayText.visible = false;
					addChild(initialsDisplayText);
				}
					
				
				if(!cityDisplayText){
					cityDisplayText = new StyleableTextField();
					cityDisplayText.visible = false;
					cityDisplayText.setStyle("color", "0xFFFFFF");
					addChild(cityDisplayText);
				}
				
				if(!stateDisplayText){
					stateDisplayText = new StyleableTextField();
					stateDisplayText.visible = false;
					stateDisplayText.setStyle("color", "0xFFFFFF");
					addChild(stateDisplayText);
				}
				
				if(!submitScoreButton){
					submitScoreButton = new Button();
					submitScoreButton.setStyle("skinClass", SubmitScoreButtonSkin);
					submitScoreButton.addEventListener(MouseEvent.MOUSE_DOWN, submitScore);
					submitScoreButton.visible = false;
					addChild(submitScoreButton);
			    }
				
			
		}
		
		/**
		 * @private
		 * 
		 * Override this method to change how the item renderer 
		 * sizes itself. For performance reasons, do not call 
		 * super.measure() unless you need to.
		 */ 
		override protected function measure():void
		{
			
			// measure all the subcomponents here and set measuredWidth, measuredHeight, 
			// measuredMinWidth, and measuredMinHeight  
			
			
			// Sets the dimensions of the entire renderer
			measuredWidth = 750;
			measuredHeight = 60;
		}
		
		/**
		 * @private
		 * 
		 * Override this method to change how the background is drawn for 
		 * item renderer.  For performance reasons, do not call 
		 * super.drawBackground() if you do not need to.
		 */
		override protected function drawBackground(unscaledWidth:Number, 
												   unscaledHeight:Number):void
		{
			
			// do any drawing for the background of the item renderer here      		
		}
		
		/**
		 * @private
		 *  
		 * Override this method to change how the background is drawn for this 
		 * item renderer. For performance reasons, do not call 
		 * super.layoutContents() if you do not need to.
		 */
		override protected function layoutContents(unscaledWidth:Number, 
												   unscaledHeight:Number):void
		{
			
			// layout all the subcomponents here    
			
		    setElementPosition(bgGroup, 0, 0);
			setElementPosition(scoreDisplayText, 65, 20);
			
			setElementSize(initialsDisplayText, 105, 54);
			setElementPosition(initialsDisplayText, 120, 20);
			
			setElementSize(initialsInputText, 105, 54);
			setElementPosition(initialsInputText, 140, 0);
			
			setElementSize(cityDisplayText, 169, 54);
			setElementPosition(cityDisplayText, 195, 20);
			
			setElementSize(cityInputText, 169, 54);
			setElementPosition(cityInputText, 250, 0);
			
			setElementSize(stateInputText, 45, 54);
			setElementPosition(stateInputText, 425, 0);
			
			setElementSize(stateDisplayText, 198, 54);
			setElementPosition(stateDisplayText, 320, 20);
			
			setElementSize(stateListButton, 198, 54);
			setElementPosition(stateListButton, 425, 0);
			
			setElementSize(submitScoreButton, 125, 55);
			setElementPosition(submitScoreButton, 625, 0);
			
			
		}
		
		
		
		private function stateClick(event:MouseEvent):void{
			
			// Shows the list of states in HighScoreDisplay (HighScoreDisplay.mxml located in the views folder)
			var st:StatePopupEvent = new StatePopupEvent(StatePopupEvent.SHOW_STATES, this);
			dispatchEvent(st);
			
			
		}
		
		private function submitScore(event:MouseEvent):void{
			
			
			// Submits the score to the database
			bgGroup.visible = false;
			initialsInputText.visible = false;
			cityInputText.visible = false;
			stateInputText.visible = false;
			submitScoreButton.visible = false;
			stateListButton.visible = false;
			
			initialsDisplayText.visible = true;
			cityDisplayText.visible = true;
			stateDisplayText.visible = true;
			
			initials = initialsInputText.text;
			city = cityInputText.text;
			state = stateInputText.text;
			
			var lbe:LeaderBoardEvent = new LeaderBoardEvent(LeaderBoardEvent.ADD_SCORE, udid, 1, "Procrit", userID, initialsInputText.text, score, cityInputText.text, stateInputText.text, 0, navigator, null);
			dispatchEvent(lbe);
			
		}
	}
}