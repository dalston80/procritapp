package com.supernovainc.sprite
{
	import flash.display.BitmapData;
	import flash.geom.Point;

	public class BlitSprite
	{
		public var sheetBmd:BitmapData;
		public var rects:Array;
		public var canvas:BitmapData;
		public var pos:Point = new Point();
		public var x:Number = 0;
		public var y:Number = 0;
		private var animIndex:int = 0;
		private var count:int = 0;
		public var animate:Boolean = false;
		
		public function BlitSprite(sheetBmd:BitmapData, rects:Array, canvas:BitmapData)
		{
			this.sheetBmd = sheetBmd;
			this.rects = rects;
			this.canvas = canvas;
		}
		
		public function render(direction:String):void
		{
			pos.x = x - rects[animIndex].width * 0.5;
			pos.y = y - rects[animIndex].height * 0.5;
			canvas.fillRect(canvas.rect, 0xFFFFFF);
			canvas.copyPixels(sheetBmd, rects[animIndex], pos, null, null, true);
			if(count%1 == 0 && animate){
				trace("animating");
				//if(direction == "forward"){
					//trace("going forward");
					animIndex = (animIndex == rects.length-1) ? rects.length-1 : animIndex+1;
				//}
			}
				 
			count++;
		}
		
	}
}