package com.supernovainc.sprite
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;

	public class ClusterR
	{
		
		[Embed(source="assets/sprites/procrit_sprite.png")]
		public static var Sheet:Class;
		
		public static var sheet:BitmapData = new Sheet().bitmapData;
		
		public static var Z6:Rectangle = new Rectangle(1556,1922,516,478);
		public static var A:Rectangle = new Rectangle(2,2,516,478);
		public static var P:Rectangle = new Rectangle(520,962,516,478);
		public static var I:Rectangle = new Rectangle(520,482,516,478);
		public static var Z8:Rectangle = new Rectangle(2592,1922,516,478);
		public static var B:Rectangle = new Rectangle(520,2,516,478);
		public static var M:Rectangle = new Rectangle(2592,482,516,478);
		public static var X:Rectangle = new Rectangle(1038,1442,516,478);
		public static var E:Rectangle = new Rectangle(2074,2,516,478);
		public static var Z93:Rectangle = new Rectangle(1038,2402,516,478);
		public static var Z:Rectangle = new Rectangle(2074,1442,516,478);
		public static var Z92:Rectangle = new Rectangle(520,2402,516,478);
		public static var R:Rectangle = new Rectangle(1556,962,516,478);
		public static var D:Rectangle = new Rectangle(1556,2,516,478);
		public static var U:Rectangle = new Rectangle(3110,962,516,478);
		public static var F:Rectangle = new Rectangle(2592,2,516,478);
		public static var Z4:Rectangle = new Rectangle(520,1922,516,478);
		public static var Z7:Rectangle = new Rectangle(2074,1922,516,478);
		public static var O:Rectangle = new Rectangle(2,962,516,478);
		public static var Z2:Rectangle = new Rectangle(3110,1442,516,478);
		public static var Z1:Rectangle = new Rectangle(2592,1442,516,478);
		public static var L:Rectangle = new Rectangle(2074,482,516,478);
		public static var W:Rectangle = new Rectangle(520,1442,516,478);
		public static var H:Rectangle = new Rectangle(2,482,516,478);
		public static var T:Rectangle = new Rectangle(2592,962,516,478);
		public static var Z91:Rectangle = new Rectangle(2,2402,516,478);
		public static var C:Rectangle = new Rectangle(1038,2,516,478);
		public static var Z5:Rectangle = new Rectangle(1038,1922,516,478);
		public static var Q:Rectangle = new Rectangle(1038,962,516,478);
		public static var K:Rectangle = new Rectangle(1556,482,516,478);
		public static var J:Rectangle = new Rectangle(1038,482,516,478);
		public static var Z3:Rectangle = new Rectangle(2,1922,516,478);
		public static var Z94:Rectangle = new Rectangle(1556,2402,516,478);
		public static var N:Rectangle = new Rectangle(3110,482,516,478);
		public static var Y:Rectangle = new Rectangle(1556,1442,516,478);
		public static var Z9:Rectangle = new Rectangle(3110,1922,516,478);
		public static var S:Rectangle = new Rectangle(2074,962,516,478);
		public static var G:Rectangle = new Rectangle(3110,2,516,478);
		public static var V:Rectangle = new Rectangle(2,1442,516,478);
		
		public static var spriteArray:Array = new Array(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, Z1, Z2, Z3, Z4, Z5, Z6, Z7, Z9, Z91, Z92, Z93, Z94);

		
		public function ClusterR()
		{
		}
	}
}