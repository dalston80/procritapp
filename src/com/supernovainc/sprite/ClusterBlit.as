package com.supernovainc.sprite
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class ClusterBlit extends Sprite
	{
		
		private var canvas:BitmapData;
		private var canvasContainer:Bitmap;
		private var zeroPoint:Point = new Point();
		private var cluster:BlitSprite;
		private var direction:String;
		
		public function ClusterBlit()
		{
			super();
			canvas = new BitmapData(516, 478, true, 0);
			canvasContainer = new Bitmap(canvas);
			addChild(canvasContainer);
			
			cluster = new BlitSprite(ClusterR.sheet, ClusterR.spriteArray, canvas);
			cluster.x = 258;
			cluster.y = 239;
			cluster.animate = false;
			addEventListener(Event.ENTER_FRAME, clusterLoop);
		}
		
		public function startUp():void{
			canvas = new BitmapData(516, 478, true, 0);
			canvasContainer = new Bitmap(canvas);
			addChild(canvasContainer);
			
			cluster = new BlitSprite(ClusterR.sheet, ClusterR.spriteArray, canvas);
			cluster.x = 258;
			cluster.y = 239;
			cluster.animate = false;
			addEventListener(Event.ENTER_FRAME, clusterLoop);
		}
		
		public function setDirection(direction:String = null):void{
			this.direction = direction;
			cluster.animate = true;
			
		}
		
		public function clusterLoop(e:Event):void{
			canvas.lock();
			
			//cluster.render("forward");
			if(cluster.animate == true){
				if(direction == "forward"){
					cluster.render("forward");
				}
			}
			
			canvas.unlock();
		}
		
		public function cleanUp():void{
			removeEventListener(Event.ENTER_FRAME, clusterLoop);
			
			if(canvasContainer){
				removeChild(canvasContainer);
				canvasContainer = null;
			}
			
			if(canvas){
				canvas = null;
			}
			
			if(cluster){
				cluster = null;
			}
			
		}
	}
}