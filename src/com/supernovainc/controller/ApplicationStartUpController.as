package com.supernovainc.controller
{
	//import com.supernova.air.extensions.device.ios.Device;
	import com.supernovainc.events.UserEvent;
	import com.supernovainc.model.ProcritAppModel;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	
	import mx.core.FlexGlobals;
	import mx.core.INavigatorContent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.rpc.remoting.mxml.RemoteObject;
	
	import org.swizframework.events.ChainEvent;
	import org.swizframework.utils.async.AsynchronousIOOperation;
	import org.swizframework.utils.async.IAsynchronousOperation;
	import org.swizframework.utils.chain.ChainType;
	import org.swizframework.utils.chain.CommandChain;
	import org.swizframework.utils.chain.EventChain;
	import org.swizframework.utils.chain.EventChainStep;
	import org.swizframework.utils.chain.FunctionChainStep;
	import org.swizframework.utils.services.ServiceHelper;
	
	import spark.components.ViewNavigator;
	
	import views.Home;
	import views.ISIScreen;
	import views.MainMenu;

	public class ApplicationStartUpController
	{
		
		/*
		The [Inject] metatdata indicates that these variable are being injected into this class
		from the BeanProvider (Beans.mxml in the configuration folder). In this case we have the
		serviceHelper, a swiz event dispatcher, our model, and remote objects
		*/
		[Inject]
		public var serviceHelper:ServiceHelper;
		
		
		[Dispatcher]
		public var dispatcher:IEventDispatcher;
		
		[Inject]
		public var appModel:ProcritAppModel;
		
		[Inject("appService")]
		public var appService:RemoteObject;
		
		[Inject("mmTitles")]
		public var mmTitles:HTTPService;
		
		
		
		private var count:int = 0;
		
		private var filesDevice:File;
		
		private var ziploader:FZip
		
		private var ue:UserEvent;
		
		private var viewNavigator:ViewNavigator;
		
		private var appNeededUpdate:Boolean = false;
		
		private var whichSteps:Array = new Array();
		
		public function ApplicationStartUpController()
		{
		}
		
		
		/* getTitles
		   Gets the title images needed for the main menu buttons (HomeScreenNavCluster.mxml located in the components folder)
		   and sets the model's titles array collection
		*/
		[EventHandler(event="TitleEvent.GET_TITLES")]
		public function getTitles():void{
			this.serviceHelper.executeServiceCall(mmTitles.send(), titleResult, titleFault);
		}
		
		private function titleResult(e:ResultEvent):void{
			if(e.result){
				appModel.titles = e.result.mm_titles.titles.title;
				
			}
		}
		
		private function titleFault(e:FaultEvent):void{
			trace("Our fault message " + e.fault.faultDetail);
		}
		
		/* end getTitles */
		
		
		
		
		
		/* getEnabled
		   This function checks with the database on the server to see if the Procrit application should be 
		   enabled and ready to use. If it is not enabled, the application will display a message indicating
		   that the application is not available
		*/
		[EventHandler(event="ApplicationModuleEvent.PROJECT_ENABLED", properties="projectID, navigator")]
		public function getEnabled(projectID:int, navigator:ViewNavigator):void{
			this.serviceHelper.executeServiceCall(appService.getEnabled(projectID), enabledResult, enabledFault, [navigator]);
		}
		
		private function enabledResult(e:ResultEvent, navigator:ViewNavigator):void{
			trace("my project enabled? " + e.result);
			if(e.result){
				appModel.projectEnabled = true;
				Home(navigator.activeView).setEnabled(appModel.projectEnabled);
				viewNavigator = navigator;
			}else{
				
			}
		}
		
		private function enabledFault(e:FaultEvent):void{
			
		}
		
		/* end getEnabled */
		
		
		
		
		
		/* getModules
		   Gets all of the application's modules from the server and stores them in a array collection
		   in the model. The result of this will eventually call initApp or initFirstRun if this is the first
	       time the application has run
		*/
		[EventHandler(event="ApplicationModuleEvent.GET_MODULES", properties="devUDID, projectID, userObj, navigator")]
		public function getModules(devUDID:String, projectID:int, userObj:Object, navigator:ViewNavigator):void{
			
			this.serviceHelper.executeServiceCall(appService.getModules(devUDID, projectID), moduleResult, moduleFault, [navigator, userObj]);
			
		}
		
		private function moduleResult(e:ResultEvent, navigator:ViewNavigator, userObj:Object):void{
			
			var modulesArray:Array = e.result as Array;
			
			// Check to make sure we have a valid array
			if(modulesArray){
				// We have a valid array, now check to see if have modules
				
				if(modulesArray.length > 0){
					// We have modules, proceed to populate the model's moduleData ArrayCollection
					appModel.moduleData.source = modulesArray;
					
					// check if this is the first time the user is running this application. If so,
					// call initFirstRun, otherwise call initApp. Both of these expect an array collection
					// as a parameter
					if(FlexGlobals.topLevelApplication.persistenceManager.getProperty("firstRun") == true){
						initFirstRun(modulesArray);
					}else{
						initApp(modulesArray);
					}
					
					
				}else{
					// Not sure what to do here yet, just testing service
					
					var test:Array = [2, 4, 6, 8];
					appModel.moduleData.source = test;
				}
				
			}else{
				// Not sure what to do here yet
			}
			
		}
		
		private function moduleFault(e:FaultEvent):void{
			trace("Our fault message " + e.fault.faultDetail);
		}
		
		/* end getModules */
		
		
		
		
		/* getUDID
		* Pulls the device UDID from the device via the AIR native extension (deviceextension.ane)
		  We request this as soon as the application starts. When testing locally, comment out the lines
		  in this function, comment out the import statement for the native extension, remove the native
		  extension in the project properties and update the model to use the hard coded UDID
		*/
		/*[EventHandler(event="UDIDEvent.GET_UDID")]
		public function getUDID():void{
			var idExt:Device = new Device();
			appModel.devUDID = idExt.getUDID();
		}*/
		/* end getUDID */
		
		
		
		
		/* initApp 
		   This function starts the process of updating the application. If there are no updates,
		   nothing is downloaded to the application and the ISIScreen (located in the views folder) is displayed
		*/
		private function initApp(mod:Array):void{
			
			// Get the number of modules
			var modules:int = mod.length;
			
			// Get the date of the last application update
			var lastUpdate:Number = FlexGlobals.topLevelApplication.persistenceManager.getProperty("lastUpdated");
			
			
			// Set up our command chain, which will allow the application to perform multiple downloads
			// if one or more modules need to be updated in the application
			var cmdChain:CommandChain = new CommandChain(ChainType.SEQUENCE, true);
			cmdChain.addEventListener(ChainEvent.CHAIN_COMPLETE, commandChainComplete, false, 0, true);
			cmdChain.addEventListener(ChainEvent.CHAIN_STEP_COMPLETE, commandChainStepComplete);
			cmdChain.addEventListener(ChainEvent.CHAIN_FAIL, commandChainFailed);
			
			
			// Loop through all of the available modules
			for(var i:int = 0; i < modules; i++){
				
				// Get the date(s) of the latest updates to the module(s)
				var dateUpdated:Date = new Date(mod[i].dateUpdated);
				
				if(lastUpdate >= dateUpdated.time){
					//No updates for that module or modules
					// The last update is current and the modules haven't been updated
					// So do nothing here
					
				}else{
					// The date on the module(s) is more recent than the last application
					// update so initiate the updating process. The command chain will add
					// a step for each module that needs an update. See ZipLoadChainStep 
					// (located in the controller folder) for how it works. Then a flag is
					// set to let the application know that there are updates
					
					cmdChain.addStep(new ZipLoadChainStep(i+1, mod[i].archiveURL));
					whichSteps.push(i);
					appNeededUpdate = true;
				}
				
			}
			
			if(appNeededUpdate){
				// The application needs to be updated so display the progress bar in Home.mxml
				// and start the command chain, which will execute all the steps in the
				// chain in sequence.
				Home(viewNavigator.activeView).showUpdating();
				cmdChain.start();
			}else{
				// The application does not need an update, so disable the command chain
				// and hide the progress indicator in Home.mxml (in the views folder. Proceed to the first view
				// which will be the ISIScreen (ISIScreen.mxml in the views folder)
				Home(viewNavigator.activeView).doneChecking();
				cmdChain.removeEventListener(ChainEvent.CHAIN_COMPLETE, commandChainComplete);
				cmdChain.removeEventListener(ChainEvent.CHAIN_FAIL, commandChainFailed);
				cmdChain.removeEventListener(ChainEvent.CHAIN_STEP_COMPLETE, commandChainStepComplete);
				cmdChain = null;
				
				viewNavigator.pushView(ISIScreen);
			}
		}
		/* end initApp */
		
		
		
		/* initFirstRun
		   If this is the first time the application is run, 
		   initFirstRun will be called to grab all of the necessary
		   files for the application
		*/
		private function initFirstRun(mod:Array):void{
			
			// Get the number of modules
			var modules:int = mod.length;
			
			// Get the date of the last application update
			var lastUpdate:Number = FlexGlobals.topLevelApplication.persistenceManager.getProperty("lastUpdated");
			
			
			// Set up our command chain, which will allow the application to perform multiple downloads
			// if one or more modules need to be updated in the application
			var cmdChain:CommandChain = new CommandChain(ChainType.SEQUENCE, true);
			cmdChain.addEventListener(ChainEvent.CHAIN_COMPLETE, commandChainComplete, false, 0, true);
			cmdChain.addEventListener(ChainEvent.CHAIN_STEP_COMPLETE, commandChainStepComplete);
			cmdChain.addEventListener(ChainEvent.CHAIN_FAIL, commandChainFailed);
			
			
			// Loop through all of the available modules
			for(var i:int = 0; i < modules; i++){
				
				// Set up the steps for the command chain
				cmdChain.addStep(new ZipLoadChainStep(i+1, mod[i].archiveURL));
				whichSteps.push(i);
				//appNeededUpdate = true;
			}
			
			// The application needs to be updated so display the progress bar in Home.mxml
			// and start the command chain, which will execute all the steps in the
			// chain in sequence.
			Home(viewNavigator.activeView).showUpdating();
			cmdChain.start();
			
		}
		/* end initFirstRun */
		
		private function commandChainComplete(e:ChainEvent):void{
			
			// The command chain is complete. Get a reference to the command chain
			var cmdChain:CommandChain = CommandChain(e.target);
			
			// Get the number of steps in the command chain
			var steps:int = cmdChain.steps.length;
			
			// Create an array from the steps array in the command chain
			var stepsArray:Array = cmdChain.steps;
			
			// Create a new module array to save to the application later
			var moduleArray:Array = new Array();
			
			
			// Check if the number of steps captured earlier in the process is less the the total number of modules and check if the application
			// already has the module content from a previous update
			if(whichSteps.length < appModel.moduleData.length && FlexGlobals.topLevelApplication.persistenceManager.getProperty("moduleContent")){
				
				// Update the existing moduleContent array 
				modulesToUpdate(FlexGlobals.topLevelApplication.persistenceManager.getProperty("moduleContent"), stepsArray);
				
			}else{
				// An update for all of the modules is required
				for(var i:int = 0; i < steps; i++){
					// Create master array of objects that will be stored on the device
					// This array contains the slides and mp3s need for the Learning section
					// (Learning.mxml located in the views folder)
					var module:Object = new Object();
					module.slides = stepsArray[i].slides;
					module.mp3s = stepsArray[i].mp3s;
					moduleArray.push(module);
				}
				
				// Save the updated moduleContent array to the application
				FlexGlobals.topLevelApplication.persistenceManager.setProperty("moduleContent", moduleArray);
			}
			
			
			// Update the date of the application's update
			FlexGlobals.topLevelApplication.persistenceManager.setProperty("lastUpdated", new Date().time);
			
			// The application has been run for the first time, so set 'firstRun' to false and save locally to
			// the application
			if(FlexGlobals.topLevelApplication.persistenceManager.getProperty("firstRun") == true){
				FlexGlobals.topLevelApplication.persistenceManager.setProperty("firstRun", false);
			}
			
			
			// Update process is now complete, proceed to the ISIScreen (ISIScreen.mxml in the views folder)
			Home(viewNavigator.activeView).doneUpdating();
			viewNavigator.pushView(ISIScreen);
			
			// Disable and remove the references to the command chain since we no longer need it
			cmdChain.removeEventListener(ChainEvent.CHAIN_COMPLETE, commandChainComplete);
			cmdChain.removeEventListener(ChainEvent.CHAIN_FAIL, commandChainFailed);
			cmdChain.removeEventListener(ChainEvent.CHAIN_STEP_COMPLETE, commandChainStepComplete);
			cmdChain = null;
			
		}
		
		private function commandChainFailed(e:ChainEvent):void{
			trace("chain failed");
		}
		
		private function commandChainStepComplete(e:ChainEvent):void{
			trace("chain step complete");
			
		}
		
		private function modulesToUpdate(existing:Array, steps:Array):void{
			// loop through all of the steps in the steps array
			for(var i:int = 0; i < steps.length; i++){
				// Based on the steps that we have, we update the modules in
				// the existing array with the slides and mp3 paths
				trace("modulesToUpdate: steps " + whichSteps[i]);
				existing[whichSteps[i]].slides = steps[i].slides;
				existing[whichSteps[i]].mp3s = steps[i].mp3s;
			}
			
			// Save the updated moduleContent array to the application
			FlexGlobals.topLevelApplication.persistenceManager.setProperty("moduleContent", existing);
		}
		
	}
}