package com.supernovainc.controller
{
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	
	import org.swizframework.utils.chain.BaseChainStep;
	import org.swizframework.utils.chain.IAutonomousChainStep;
	
	public class ZipLoadChainStep extends BaseChainStep implements IAutonomousChainStep
	{
		
		public var zipID:int = 0;
		public var zipURL:String;
		
		// This is a reference to the FZip ActionScript library
		// which allows an application to download and extract
		// zip files. For more information, go to http://codeazur.com.br/lab/fzip/
		public var zip:FZip;
		
		
		public var filesDevice:File; 
		public var slides:Array;
		public var mp3s:Array;
		
		public function ZipLoadChainStep(zid:int, zurl:String)
		{
			super();
			
			// Set up variables 
			zipID = zid;
			zipURL = zurl;
			zip = new FZip();
			slides = new Array();
			mp3s = new Array();
			zip.addEventListener(Event.COMPLETE, zipCompleteHandler);
		}
		
		public function doProceed():void
		{
			// Start the download of the zip file and start this step in the 
			// command chain
			zip.load(new URLRequest(zipURL));
		}
		
		private function zipCompleteHandler(e:Event):void{
			
			// Set up the download process and application storage directory,
			// where the extracted files will be saved
			var files:FZipFile;
			var stream:FileStream = new FileStream();
			filesDevice = File.applicationStorageDirectory;
			
			// Loop through all the files in the zip file
			for(var i:int = 0; i < zip.getFileCount(); i++){
				
				// Get each zip file
				files = zip.getFileAt(i);
				
				
				// Check within the zip file for the "slides" folder and files in that folder that
				// have an extension of ".png"
				if(files.filename.indexOf("slides/") == 0 && files.filename.indexOf(".png") != -1){
					// Set up the exact path in the application where the files will be saved
					var f:File = filesDevice.resolvePath("moduleContent"+zipID+"/"+files.filename);
					
					//start writing the data to the application
					stream.open(f, FileMode.WRITE);
					stream.writeBytes(files.content);
					
					// add the path to the file to the slides array
					slides.push(f.url);
					
					// the file has been written and saved
					stream.close();
					
				}
				
				
				// Check within the zip file for the "mp3s" folder and files in that folder that
				// have an extension of ".mp3"
				if(files.filename.indexOf("mp3s/") == 0 && files.filename.indexOf(".mp3") != -1){
					// Set up the exact path in the application where the files will be saved
					f = filesDevice.resolvePath("moduleContent"+zipID+"/"+files.filename);
					
					//start writing the data to the application
					stream.open(f, FileMode.WRITE);
					stream.writeBytes(files.content);
					
					
					// add the path to the file to the slides array
					mp3s.push(f.url);
					
					// the file has been written and saved
					stream.close();
				}
				
			}
			
			// This completes the chain step in the command chain and proceed to the next step if there are more updates
			complete();
		}
	}
}