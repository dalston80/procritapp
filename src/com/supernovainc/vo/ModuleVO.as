package com.supernovainc.vo
{
	public class ModuleVO
	{
		
		public var moduleID:int;
		public var name:String;
		public var enabled:Boolean;
		public var archiveURL:String;
		public var childModules:Array;
		public var dateUpdated:String;
		
		public function ModuleVO()
		{
		}
	}
}