package com.supernovainc.vo
{
	public class UserVO
	{
		
		public var userID:int;
		public var fullName:String;
		public var title:String;
		public var avatarURL:String;
		public var progress:int;
		
		public function UserVO()
		{
		}
		
	}
}