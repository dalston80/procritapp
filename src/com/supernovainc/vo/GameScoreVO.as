package com.supernovainc.vo
{
	public class GameScoreVO
	{
		
		public var initials:String;
		public var challengeScore:String;
		public var city:String;
		public var state:String;
		
		public function GameScoreVO()
		{
		}
	}
}