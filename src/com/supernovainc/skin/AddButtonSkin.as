package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class AddButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/add_profile_btn.png")]
		private var Add_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/add_profile_touch_btn.png")]
		private var Add_Button_Skin_Down:Class;
		
		public function AddButtonSkin()
		{
			super();
			upBorderSkin = Add_Button_Skin;
			downBorderSkin = Add_Button_Skin_Down;
			width = 119;
			height = 34;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}