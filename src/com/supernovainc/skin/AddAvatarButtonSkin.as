package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class AddAvatarButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/add_btn.png")]
		private var Addav_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/add_touch_btn.png")]
		private var Addav_Button_Skin_Down:Class;
		
		public function AddAvatarButtonSkin()
		{
			super();
			upBorderSkin = Addav_Button_Skin;
			downBorderSkin = Addav_Button_Skin_Down;
			width = 125;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}