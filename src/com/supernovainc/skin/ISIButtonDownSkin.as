package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ISIButtonDownSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/indication_touch_btn.png")]
		private var PI_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/indication_touch_btn.png")]
		private var PI_Button_Skin_Down:Class;
		
		public function ISIButtonDownSkin()
		{
			super();
			upBorderSkin = PI_Button_Skin;
			downBorderSkin = PI_Button_Skin_Down;
			width = 171;
			height = 57;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}