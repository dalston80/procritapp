package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class DosingToolButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Resources/Dosing_tool_btn.png")]
		private var DT_Button_Skin:Class;
		
		[Embed(source="assets/Resources/Dosing_tool_btn_touch.png")]
		private var DT_Button_Skin_Down:Class;
		
		public function DosingToolButtonSkin()
		{
			super();
			upBorderSkin = DT_Button_Skin;
			downBorderSkin = DT_Button_Skin_Down;
			width = 151;
			height = 56;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}