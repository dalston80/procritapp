package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class SelectImageButtonDownSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/Drop down/drop_down_touch.png")]
		private var SI_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/Drop down/drop_down_touch.png")]
		private var SI_Button_Skin_Down:Class;
		
		public function SelectImageButtonDownSkin()
		{
			super();
			upBorderSkin = SI_Button_Skin;
			downBorderSkin = SI_Button_Skin_Down;
			width = 268;
			height = 233;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}