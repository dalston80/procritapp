package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PreviousLearningButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/left_arrow.png")]
		private var PrevL_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/left_arrow_touch.png")]
		private var PrevL_Button_Skin_Down:Class;
		
		public function PreviousLearningButtonSkin()
		{
			super();
			upBorderSkin = PrevL_Button_Skin;
			downBorderSkin = PrevL_Button_Skin_Down;
			width = 48;
			height = 48;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}