package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class UpdateProfileButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/update_profile_btn.png")]
		private var UP_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/update_profile_btn_touch.png")]
		private var UP_Button_Skin_Down:Class;
		
		public function UpdateProfileButtonSkin()
		{
			super();
			upBorderSkin = UP_Button_Skin;
			downBorderSkin = UP_Button_Skin_Down;
			width = 181;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}