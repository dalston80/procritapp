package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class EditButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/edit_profile_btn.png")]
		private var Edit_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/edit_profile_touch_btn.png")]
		private var Edit_Button_Skin_Down:Class;
		
		public function EditButtonSkin()
		{
			super();
			upBorderSkin = Edit_Button_Skin;
			downBorderSkin = Edit_Button_Skin_Down;
			width = 119;
			height = 34;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}