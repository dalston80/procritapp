package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class CreateProfileButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/create_profile_btn.png")]
		private var CP_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/create_profile_touch_btn.png")]
		private var CP_Button_Skin_Down:Class;
		
		public function CreateProfileButtonSkin()
		{
			super();
			upBorderSkin = CP_Button_Skin;
			downBorderSkin = CP_Button_Skin_Down;
			width = 181;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}