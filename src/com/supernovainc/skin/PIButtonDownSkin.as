package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PIButtonDownSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/prescribing_touch_btn.png")]
		private var PI_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/prescribing_touch_btn.png")]
		private var PI_Button_Skin_Down:Class;
		
		public function PIButtonDownSkin()
		{
			super();
			upBorderSkin = PI_Button_Skin;
			downBorderSkin = PI_Button_Skin_Down;
			width = 145;
			height = 57;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}