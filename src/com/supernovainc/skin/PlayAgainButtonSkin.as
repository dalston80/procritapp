package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PlayAgainButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/High Score/play_again_btn.png")]
		private var PA_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/High Score/play_again_btn_touch.png")]
		private var PA_Button_Skin_Down:Class;
		
		public function PlayAgainButtonSkin()
		{
			super();
			upBorderSkin = PA_Button_Skin;
			downBorderSkin = PA_Button_Skin_Down;
			width = 125;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}