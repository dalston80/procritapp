package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class SubmitScoreButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/High Score/submit_btn.png")]
		private var Sub_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/High Score/submit_btn_touch.png")]
		private var Sub_Button_Skin_Down:Class;
		
		public function SubmitScoreButtonSkin()
		{
			super();
			upBorderSkin = Sub_Button_Skin;
			downBorderSkin = Sub_Button_Skin_Down;
			width = 125;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}