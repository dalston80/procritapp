package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PauseButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/pause_btn.png")]
		private var Pause_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/pause_btn.png")]
		private var Pause_Button_Skin_Down:Class;
		
		public function PauseButtonSkin()
		{
			super();
			upBorderSkin = Pause_Button_Skin;
			downBorderSkin = Pause_Button_Skin_Down;
			width = 23;
			height = 38;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}