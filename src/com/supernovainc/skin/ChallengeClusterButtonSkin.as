package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ChallengeClusterButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/challenge_btn_up.png")]
		private var Cluster_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/challenge_btn_down.png")]
		private var Cluster_Button_Skin_Down:Class;
		
		public function ChallengeClusterButtonSkin()
		{
			super();
			upBorderSkin = Cluster_Button_Skin;
			downBorderSkin = Cluster_Button_Skin_Down;
			width = 126;
			height = 127;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}