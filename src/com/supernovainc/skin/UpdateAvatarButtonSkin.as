package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class UpdateAvatarButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/update_image_btn.png")]
		private var UP_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/update_image_btn.png")]
		private var UP_Button_Skin_Down:Class;
		
		public function UpdateAvatarButtonSkin()
		{
			super();
			upBorderSkin = UP_Button_Skin;
			downBorderSkin = UP_Button_Skin_Down;
			width = 139;
			height = 30;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}