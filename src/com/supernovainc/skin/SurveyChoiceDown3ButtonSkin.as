package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class SurveyChoiceDown3ButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/FB_3_ON.png")]
		private var sc_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/FB_3_ON.png")]
		private var sc_Button_Skin_Down:Class;
		
		public function SurveyChoiceDown3ButtonSkin()
		{
			super();
			upBorderSkin = sc_Button_Skin;
			downBorderSkin = sc_Button_Skin_Down;
			width = 45;
			height = 45;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}