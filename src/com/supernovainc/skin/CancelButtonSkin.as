package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class CancelButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/Delete Profile pop-up/cancel_btn.png")]
		private var CC_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/Delete Profile pop-up/cancel_touch_btn.png")]
		private var CC_Button_Skin_Down:Class;
		
		public function CancelButtonSkin()
		{
			super();
			upBorderSkin = CC_Button_Skin;
			downBorderSkin = CC_Button_Skin_Down;
			width = 141;
			height = 50;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}