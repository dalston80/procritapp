package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PreviousButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/previous_btn.png")]
		private var Prev_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/previous_btn_touch.png")]
		private var Prev_Button_Skin_Down:Class;
		
		public function PreviousButtonSkin()
		{
			super();
			upBorderSkin = Prev_Button_Skin;
			downBorderSkin = Prev_Button_Skin_Down;
			width = 121;
			height = 138;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}