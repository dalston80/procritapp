package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class LearningClusterButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/learning_btn_up.png")]
		private var Cluster_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/learning_btn_down.png")]
		private var Cluster_Button_Skin_Down:Class;
		
		public function LearningClusterButtonSkin()
		{
			super();
			upBorderSkin = Cluster_Button_Skin;
			downBorderSkin = Cluster_Button_Skin_Down;
			width = 125;
			height = 126;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}