package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class StateDropDownButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/High Score/state_dropdown.png")]
		private var SD_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/High Score/state_dropdown.png")]
		private var SD_Button_Skin_Down:Class;
		
		public function StateDropDownButtonSkin()
		{
			super();
			upBorderSkin = SD_Button_Skin;
			downBorderSkin = SD_Button_Skin_Down;
			width = 198;
			height = 54;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}