package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class NextLearningButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/right_arrow.png")]
		private var NextL_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/right_arrow_touch.png")]
		private var NextL_Button_Skin_Down:Class;
		
		public function NextLearningButtonSkin()
		{
			super();
			upBorderSkin = NextL_Button_Skin;
			downBorderSkin = NextL_Button_Skin_Down;
			width = 48;
			height = 48;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}