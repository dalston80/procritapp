package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class DeleteProfileButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/delete_profile_btn.png")]
		private var DP_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/delete_profile_touch_btn.png")]
		private var DP_Button_Skin_Down:Class;
		
		public function DeleteProfileButtonSkin()
		{
			super();
			upBorderSkin = DP_Button_Skin;
			downBorderSkin = DP_Button_Skin_Down;
			width = 181;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}