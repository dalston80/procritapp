package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ClusterButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/btn_red_bar.png")]
		private var Cluster_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/btn_red_bar.png")]
		private var Cluster_Button_Skin_Down:Class;
		
		public function ClusterButtonSkin()
		{
			super();
			upBorderSkin = Cluster_Button_Skin;
			downBorderSkin = Cluster_Button_Skin_Down;
			width = 120;
			height = 38;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}