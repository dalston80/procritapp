package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ProfileISIButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/Profile_R-Arrow.png")]
		private var P_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/Profile_R-Arrow.png")]
		private var P_Button_Skin_Down:Class;
		
		public function ProfileISIButtonSkin()
		{
			super();
			upBorderSkin = P_Button_Skin;
			downBorderSkin = P_Button_Skin_Down;
			width = 137;
			height = 64;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}