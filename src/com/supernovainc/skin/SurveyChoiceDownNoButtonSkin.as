package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class SurveyChoiceDownNoButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/FB_No_ON.png")]
		private var sc_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/FB_No_ON.png")]
		private var sc_Button_Skin_Down:Class;
		
		public function SurveyChoiceDownNoButtonSkin()
		{
			super();
			upBorderSkin = sc_Button_Skin;
			downBorderSkin = sc_Button_Skin_Down;
			width = 50;
			height = 50;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}