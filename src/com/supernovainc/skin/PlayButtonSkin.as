package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class PlayButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/play_btn.png")]
		private var Play_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/play_btn.png")]
		private var Play_Button_Skin_Down:Class;
		
		public function PlayButtonSkin()
		{
			super();
			upBorderSkin = Play_Button_Skin;
			downBorderSkin = Play_Button_Skin_Down;
			width = 34;
			height = 38;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}