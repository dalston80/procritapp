package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class SelectImageButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/Drop down/drop_down.png")]
		private var SI_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/Drop down/drop_down.png")]
		private var SI_Button_Skin_Down:Class;
		
		public function SelectImageButtonSkin()
		{
			super();
			upBorderSkin = SI_Button_Skin;
			downBorderSkin = SI_Button_Skin_Down;
			width = 268;
			height = 54;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}