package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class HomeButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/Home_btn.png")]
		private var sb_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/FeedBACK PNGs/Home_btn.png")]
		private var sb_Button_Skin_Down:Class;
		
		public function HomeButtonSkin()
		{
			super();
			upBorderSkin = sb_Button_Skin;
			downBorderSkin = sb_Button_Skin_Down;
			width = 135;
			height = 62;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}