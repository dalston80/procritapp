package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ScrubButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/progress_bar_btn.png")]
		private var Scrub_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/progress_bar_btn.png")]
		private var Scrub_Button_Skin_Down:Class;
		
		public function ScrubButtonSkin()
		{
			super();
			upBorderSkin = Scrub_Button_Skin;
			downBorderSkin = Scrub_Button_Skin_Down;
			width = 27;
			height = 27;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}