package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ClosePopupButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Learning Elements/close_btn.png")]
		private var CP_Button_Skin:Class;
		
		[Embed(source="assets/Learning Elements/close_btn.png")]
		private var CP_Button_Skin_Down:Class;
		
		public function ClosePopupButtonSkin()
		{
			super();
			upBorderSkin = CP_Button_Skin;
			downBorderSkin = CP_Button_Skin_Down;
			width = 141;
			height = 50;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}