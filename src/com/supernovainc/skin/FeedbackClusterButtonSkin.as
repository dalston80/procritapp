package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class FeedbackClusterButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/feedback_btn_up.png")]
		private var Feed_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/feedback_btn_down.png")]
		private var Feed_Button_Skin_Down:Class;
		
		public function FeedbackClusterButtonSkin()
		{
			super();
			upBorderSkin = Feed_Button_Skin;
			downBorderSkin = Feed_Button_Skin_Down;
			width = 128;
			height = 127;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}