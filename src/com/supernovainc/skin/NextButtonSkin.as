package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class NextButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/next_btn.png")]
		private var Next_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/next_btn_touch.png")]
		private var Next_Button_Skin_Down:Class;
		
		public function NextButtonSkin()
		{
			super();
			upBorderSkin = Next_Button_Skin;
			downBorderSkin = Next_Button_Skin_Down;
			width = 117;
			height = 138;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}