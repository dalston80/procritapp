package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ProfilesButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Splash screen/profiles_btn.png")]
		private var P_Button_Skin:Class;
		
		[Embed(source="assets/Splash screen/profiles_btn_touch.png")]
		private var P_Button_Skin_Down:Class;
		
		public function ProfilesButtonSkin()
		{
			super();
			upBorderSkin = P_Button_Skin;
			downBorderSkin = P_Button_Skin_Down;
			width = 134;
			height = 60;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}