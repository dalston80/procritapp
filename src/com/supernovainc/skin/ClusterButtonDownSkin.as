package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class ClusterButtonDownSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Main menu elements/btn_red_bar_touch.png")]
		private var Cluster_Button_Skin:Class;
		
		[Embed(source="assets/Main menu elements/btn_red_bar_touch.png")]
		private var Cluster_Button_Skin_Down:Class;
		
		public function ClusterButtonDownSkin()
		{
			super();
			upBorderSkin = Cluster_Button_Skin;
			downBorderSkin = Cluster_Button_Skin_Down;
			width = 121;
			height = 117;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}