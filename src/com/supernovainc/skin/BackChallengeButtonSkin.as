package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class BackChallengeButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Challenge/High Score/back_btn.png")]
		private var BC_Button_Skin:Class;
		
		[Embed(source="assets/Challenge/High Score/back_btn_touch.png")]
		private var BC_Button_Skin_Down:Class;
		
		public function BackChallengeButtonSkin()
		{
			super();
			upBorderSkin = BC_Button_Skin;
			downBorderSkin = BC_Button_Skin_Down;
			width = 125;
			height = 55;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}