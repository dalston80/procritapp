package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class BackButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/back_btn.png")]
		private var Back_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/back_touch_btn.png")]
		private var Back_Button_Skin_Down:Class;
		
		public function BackButtonSkin()
		{
			super();
			upBorderSkin = Back_Button_Skin;
			downBorderSkin = Back_Button_Skin_Down;
			width = 68;
			height = 34;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}