package com.supernovainc.skin
{
	import spark.skins.mobile.ButtonSkin;
	
	public class DeleteProfileConfirmButtonSkin extends ButtonSkin
	{
		
		[Embed(source="assets/Profile screens/Delete Profile pop-up/delete_profile2_btn.png")]
		private var DP_Button_Skin:Class;
		
		[Embed(source="assets/Profile screens/Delete Profile pop-up/delete_profile2_touch_btn.png")]
		private var DP_Button_Skin_Down:Class;
		
		public function DeleteProfileConfirmButtonSkin()
		{
			super();
			upBorderSkin = DP_Button_Skin;
			downBorderSkin = DP_Button_Skin_Down;
			width = 190;
			height = 50;
			
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void{
			
		}
	}
}