package com.supernovainc.model
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class ProcritAppModel
	{
		/*users
		   stores all of the user data for the application
		*/
		public var users:ArrayCollection = new ArrayCollection();
		
		
		/* 
		   stores all of the high scores for the challenge section
		*/
		public var leaderBoardScores:ArrayCollection = new ArrayCollection();
		
		
		/* 
		   stores all of the module data for each of the four modules of this application
		*/
		public var moduleData:ArrayCollection = new ArrayCollection();
		
		
		/* 
		   This is the hard coded UDID of Supernova's test iPad 2
		   use this when testing the app locally on the desktop
		*/
		public var devUDID:String = "079281fba21ad9e40f8db082706d6de7004c6ae7";
		
		
		/* This is populated dynamically by the iPad via a call to the
		   device native extension (deviceextension.ane located in the ane folder)
		   Uncomment when ready to test on device (and comment out the above)
		*/
		//public var devUDID:String;
		
		
		
		/* 
		
		learningData, challengeData, resourceData
		 
		These are no longer being used and have been replaced by currentUser/currentModule
		
		public var learningData:Object;
		
		public var challengeData:Object;
		
		public var resourceData:Object;
		
		*/
		
		
		
		
		/* 
		   Stores the data for the currentUser
		*/
		public var currentUser:Object;
		
		
		/*
		   Stores the data for the currentModule
		*/
		public var currentModule:Object;
		
		
		
		/*
		   Flag indicating whether or not the app is enabled for use
		*/
		public var projectEnabled:Boolean;
		
		public var testString:String;
		
		
		
		/*
		  Stores the paths to the title images used in the main menu for the main menu buttons
		*/
		public var titles:ArrayCollection = new ArrayCollection();
		
		//public var resourceLinks:ArrayCollection = new ArrayCollection(); No need for this collection since the content is now embedded
		
		//public var surveyQuestions:ArrayCollection = new ArrayCollection();  No need for this collection since the content is now embedded
		
		
		/* 
		  This array has the paths to the quiz questions that are used in the challenge section. The challenge section loads 
		  one of these based on what module you are viewing
		*/
		public var quizQuestions:Array = ["xml/questions_BM.xml", "xml/questions_CTR.xml", "xml/questions_DOS.xml"];
		
		public function ProcritAppModel()
		{
		}
	}
}